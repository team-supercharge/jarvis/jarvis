# JARVIS

This repository contains [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) scripts you can use to conveniently bootstrap the CI/CD pipelines for your projects.

* [Background](#background)
  * [Pipeline Phases](#pipeline-phases)
  * [General Approach](#general-approach)
* [Getting Started](#getting-started)
  * [Including JARVIS](#including-jarvis)
  * [Setting up the Environment](#setting-up-the-environment)
    * [Resource requests and limits](#resource-requests-and-limits)
* [Template  Directory](#template-directory)
* [Templates](#templates)
  * [Base Templates](#base-templates)
  * [Release Templates](#release-templates)
  * [Linter Templates](#linter-templates)
  * [Parallel Templates](#parallel-templates)
  * [Docker Templates](#docker-templates)
  * [Renovate Templates](#renovate-templates)
  * [Notification Templates](#notification-templates)
  * [Node.js Templates](#nodejs-templates)
  * [Image Templates](#image-templates)
  * [Util Templates](#util-templates)
  * [Script Templates](#script-templates)
* [Migration Guide](#migration-guide)
* [Contributing](#contributing)
* [Versioning](#versioning)

---

# Background

## Pipeline Phases

```
         Merge Request                       Release                  Build         Deploy
        ┌─────────────────────────┐        ┌──────────────────┐        ┌─────────────┬───────┐
        │       ┌───┐             │        │ ┌──────────────┐ │        │ ┌───┐         ┌───┐ │
        │       │job│             │        │ │stable release│ │        │ │job│       │ │job│ │
 auto   │ ┌───┐ └───┘ ┌───┐ ┌───┐ │ manual │ └──────────────┘ │ auto   │ └───┘ ┌───┐   └───┘ │
───────►│ │job│       │job│ │job│ │───────►│ ┌────────────┐   │───────►│       │job│ │       │
 on MR  │ └───┘ ┌───┐ └───┘ └───┘ │        │ │pre release │   │ on tag │ ┌───┐ └───┘   ┌───┐ │
        │       │job│             │        │ └────────────┘   │        │ │job│       │ │job│ │
        │       └───┘             │        │ ┌────────────┐   │        │ └───┘         └───┘ │
        └─────────────────────────┘        │ │dev release │   │        └─────────────┴───────┘
                                           │ └────────────┘   │            │             ▲
                                           └──────────────────┘            │  ┌────────┐ │
                                                                           └─►│artifact│─┘
                                                                              └────────┘
```

At [Supercharge](https://supercharge.io/) we stick to the _"keep master green"_ approach and rely heavily on checking code health on **Merge Requests** as a first phase of our CI processes.

When we have enough features or fixes to release, we use a manually triggered, but otherwise fully automated **Release** process to produce a new release (represented by a Git commit and a tag) with a calculated [Semantic Version](https://semver.org/) which can be built into stable artifacts.

The next phase is the **Build** phase where the release artifacts are generated and pushed into long-term artifact storages.

And finally comes the **Deploy** phase in which the releases are distributed to their final environments based on the platform the project is targeted to. Technically the Deploy phase is merged to a single pipeline with the Build phase with a manual trigger separating the two.

## General Approach

### Job Templates

JARVIS **does not create any jobs for you**, but provides convenient **job templates** you can use to set up the CI/CD for your project in a heartbeat!

You can use these templates by creating a new job in your `.gitlab-ci.yml` file and extending from one of these templates, like this:

```yaml
include:
  - project: team-supercharge/jarvis/jarvis
    file: jarvis.yml
    ref: <version>

your job:
    extends: .jarvis-<job-template>
```

Please check the `.gitlab-ci.yml` file in this repository for a concrete example, as JARVIS uses itself for setting up its pipelines.

### `before_script` and `after_script` blocks

Apart from `.jarvis-main-release`,`.jarvis-hotfix-release`, `.jarvis-pre-release` and `.jarvis-temp-release` every template is purposefully kept clean of `before_script` and `after_script` blocks, so you're free to use them to extend job functionality without `!reference`-ing the main `script` tag.

### Retry Logic

By default JARVIS applies a default retry logic with the maximum of 2 retries on `runner_system_failure` events. Please take this into consideration when you need specific retry rules in your custom jobs.

&nbsp;

# Getting Started

## Including JARVIS

When your project is hosted on gitlab.com JARVIS should always be included in your projects `.gitlab-ci.yml` by explicitly specifying a version, like this:

```yaml
include:
  - project: team-supercharge/jarvis/jarvis
    file: jarvis.yml
    ref: <version tag>
```

when your project is hosted on a self-managed GitLab instance use the following snippet:

```yaml
include:
  - https://gitlab.com/team-supercharge/jarvis/jarvis/raw/<version-tag>/jarvis.yml
```

## Setting up the Environment

The following environment variables need to be set in order for JARVIS to work:

* `JARVIS_GIT_SSH_KEY` - the SSH key JARVIS will use to commit changes to repositories; recommended to use an SSH key in [Ed25519](https://ed25519.cr.yp.to/) format to fit a single variable
* `JARVIS_GIT_USER_EMAIL` - the email address used for commits
* `JARVIS_GIT_USER_NAME` - the name used for commits

> ⚠️ The variables above should be considered to be created as global variables (in a self-managed GitLab instance) or group-level variables (on GitLab.com) to avoid unnecessary configuration steps for new projects.

The following variables are optional to set:

* `JARVIS_DOCKER_HUB_USERNAME` - username to authenticate towards DockerHub **in the `.jarvis-docker-build` templates** (authentication for the job base images need to be configured on a GitLab runner level!)
* `JARVIS_DOCKER_HUB_PASSWORD` - password for DockerHub authentication

### Resource requests and limits

We run Jarvis based jobs in supercharge on a kubernetes cluster leveraging GitLab runner's [kubernetes support](https://docs.gitlab.com/runner/executors/kubernetes.html).
To differentiate where the CI job's pod is scheduled we use these two tags. One of them MUST be specified for your job
to be scheduled.

* `amd64` - the job should be allocated to a node with an amd64 (x86_64) processor
* `arm64` - the job should be scheduled on a node with an arm64 (aarch64) processor

To request a specific pod size (CPU and memory allocation) for your job you should extend one of these job templates:

* `.jarvis-pod-size-s`: CPU: 0.95 core, RAM: 462Mi
* `.jarvis-pod-size-m`: CPU: 0.95 core, RAM: 1.85Gi
* `.jarvis-pod-size-l`: CPU: 0.95 core, RAM: 3.7Gi
* `.jarvis-pod-size-xl`: CPU: 1.9 core, RAM: 7.5Gi

Failing to do so makes your pod size default to the smallest one. Please try to use these templates if you can, as they
are carefully tuned to result in optimal resource allocation. If you have more specific needs (like more memory) for
your builds, then you can request more by overriding the `KUBERNETES_CPU_REQUEST`, `KUBERNETES_CPU_LIMT`,
`KUBERNETES_MEMORY_REQUEST` and `KUBERNETES_MEMORY_LIMIT` variables. See the relevant documentation
[here](https://docs.gitlab.com/runner/executors/kubernetes.html#overwriting-container-resources).

&nbsp;

# Template Directory

Every job you define in your pipeline need to belong to a certain [Phase](#pipeline-phases). The table below summarizes the base templates of each phase. The _Custom_ value signifies if an arbitrary number of jobs can be added to the given phase using its Base Template.

| Phase | Base Template(s) | Custom | Comment |
|-------|------------------|--------|---------|
| **MR** | `.jarvis-on-merge-request` | ✅ | every job that runs in the MR Phase |
| **Release** | `.jarvis-main-release` <br />`.jarvis-pre-release` <br />`.jarvis-hotfix-release` | ❌ | these templates need to be added to the pipeline **exactly once** |
| **Build** | `.jarvis-on-release` | ✅ | every job that is used to build a release to an artifact |
| **Deploy** | `.jarvis-on-release` | ✅ | after the artifact generated in the **Build** phase _manual_ jobs are used to trigger deployments |

In the following table you can see every available JARVIS template and script and in which Phase they can be used in.

| Template | MR | Release | Build | Deploy | Summary | Size |
|----------|----|---------|-------|--------|---------|------|
| [**Base Templates**](#base-templates) | | | | | | |
| `.jarvis-on-merge-request` | ✅ | ❌ | ❌ | ❌ | base template for built-in and custom MR jobs | - |
| `.jarvis-on-XXX-release` | ❌ | ❌ | ✅ | ✅ | base templates for jobs to run on specific releases | - |
| [**Release Templates**](#release-templates) | | | | | | |
| `.jarvis-main-release` | ❌ | ✅ | ❌ | ❌ | manually trigger a _main release_ | `S` |
| `.jarvis-hotfix-release` | ❌ | ✅ | ❌ | ❌ | manually trigger a _hotfix release_ | `S` |
| `.jarvis-pre-releaes` | ❌ | ✅ | ❌ | ❌ | manually trigger a _pre release_ | `S` |
| `.jarvis-temp-releaes` | ❌ | ✅ | ❌ | ❌ | manually trigger a _temp release_ | `S` |
| [**Linter Templates**](#linter-templates) | | | | | | |
| `.jarvis-commitlint` | ✅ | ❌ | ❌ | ❌ | lint conventional commits | `S` |
| `.jarvis-docker-lint` | ✅ | ❌ | ❌ | ❌ | lint Dockerfiles | `S` |
| `.jarvis-parallel-docker-lint` | ✅ | ❌ | ❌ | ❌ | lint Dockerfiles in parallel | `S` |
| `.jarvis-lockfile-lint` | ✅ | ❌ | ❌ | ❌ | lint Node.js package lock files | `S` |
| `.jarvis-renovate-lint` | ✅ | ❌ | ❌ | ❌ | lint [Renovate](#renovate-templates) configuration files | `S` |
| [**Parallel Templates**](#parallel-templates) | | | | | | |
| `.jarvis-parallel` | ✅ | ❌ | ✅ | N/A | trigger matrix builds for monorepos | | - |
| `.jarvis-parallel-on-demand` | ✅ | ❌ | ❌ | N/A | trigger builds in monorepos only for changed project | - |
| [**Docker Templates**](#docker-templates) | | | | | | |
| `.jarvis-docker-build` | ✅ | ❌ | ✅ | N/A | build Docker images | - |
| `.jarvis-parallel-docker-build` | ✅ | ❌ | ✅ | N/A | build Docker images for monorepos | - |
| [**Renovate Templates**](#renovate-templates) | | | | | | |
| `.jarvis-renovate` | N/A | N/A | N/A | N/A | run [Renovate](#renovate-templates) _scheuled_ for automated dependency updates | - |
| [**Notification Templates**](#notification-templates) | | | | | | |
| `.jarvis-notify-build-start` | ❌ | ❌ | ✅ | ❌ | triggers a "build start" notification | `S` |
| `.jarvis-notify-build-success` | ❌ | ❌ | ✅ | ❌ | marks previous notification as "success" | `S` |
| `.jarvis-notify-build-failed` | ❌ | ❌ | ✅ | ❌ | marks previous notification as "failed" | `S` |
| `.jarvis-send-message` | ✅ | ✅ | ✅ | ✅ | sends custom defined message | `S` |
| [**Node.js Templates**](#nodejs-templates) | | | | | | |
| `.jarvis-node-preheat-cache` | ✅ | ❌ | ✅ | N/A | handle Node.js cache and invalidation | - |
| [**Image Templates**](#image-templates) | | | | | | |
| `.jarvis-deploy` | N/A | ❌ | N/A | ✅ | tools for k8s deployment | - |
| `.jarvis-docker-cli` | ✅ | ❌ | ✅ | N/A | `docker-cli` preinstalled in the image | - |
| [**Util Templates**](#util-templates) | | | | | | |
| `.jarvis-jacoco-to-cobertura` | ✅ | ❌ | ✅ | N/A | convert [JaCoCo](https://www.eclemma.org/jacoco/) test reports to [Cobertura](https://cobertura.github.io/cobertura/) format | `S` |
| [**Script Templates**](#script-templates) | | | | | | |
| `.jarvis-extract-release-version` | ❌ | ❌ | ✅ | ✅ | parse version info from last release | N/A |
| `.jarvis-extract-release-changes` | ❌ | ❌ | ✅ | ✅ | parse changes from last release | N/A |
| `.jarvis-extract-release-changes-since-last-deployment` | ❌ | ❌ | ✅ | ✅ | parse changes since last deploy | N/A |
| `.jarvis-publish-generic-package` | ❌ | ❌ | ✅ | ✅ | publish file as a [generic package](https://docs.gitlab.com/ee/user/packages/generic_packages/) | N/A |
| `.jarvis-download-generic-package` | ❌ | ❌ | ✅ | ✅ | download file published in a [generic package](https://docs.gitlab.com/ee/user/packages/generic_packages/) | N/A |

&nbsp;

# Templates

## Base Templates

### `.jarvis-on-merge-request`

The `.jarvis-on-merge-request` should used be extended for any custom merge request job. The template takes care of providing the basic `rules` for running the job only on merge requests. To add more rules if needed use the `!reference` syntax the following way:

```yaml
build-check:
  extends: .jarvis-on-merge-request
  script:
    - npm run build
  rules:
    - // add your custom rules //
    - !reference [.jarvis-on-merge-request, rules]
```

> ⚠️ Please note, that `rules` entries are evaulated in the order they are defined, so adding a "matching" rule before the default ruleset might result in adding the job to non-MR pipelines as well! If you do need to add more rules, consider writing them from scratch e.g. `- if: '$CI_PIPELINE_SOURCE == "merge_request_event" && <some other condition>'`

It is a good practice to treat every job running on an MR as `interruptible` ([see more](https://docs.gitlab.com/ee/ci/yaml/#interruptible)) to save CI resources on new pushes or rebases. The `.jarvis-on-merge-request` template takes care of uniformly flagging all jobs as `interruptible: true`.

### `.jarvis-on-XXX-release`

You need to make your **Build** / **Deploy** pipeline automatically triggered when a new release (Git tag with a specific format) is ready to be built. The created tags correspond to the [Release Templates](#release-templates).

There are multiple release **types** and **tiers** defined and you can make your pipeline triggered in a flexible way. The following table summarizes the release types and tiers each having their individual and composite templates.

<table>
  <tr>
    <th colspan="1">tag prefix</th>
    <th colspan="2">tier</th>
    <th colspan="2">type</th>
  </tr>
  <tr>
    <td rowspan="2">
      <code>v</code>
    </td>
    <td rowspan="2">
      <i>stable</i>
    </td>
    <td rowspan="2">
      <code>.jarvis-on-stable-release</code>
    </td>
    <td><i>main</i></td>
    <td><code>.jarvis-on-main-release</code></td>
  </tr>
  <tr>
    <td><i>hotfix</i></td>
    <td><code>.jarvis-on-hotfix-release</code></td>
  </tr>
  <tr>
    <td rowspan="3">
      <code>v</code>
    </td>
    <td rowspan="3">
      <i>pre</i>
    </td>
    <td rowspan="3">
      <code>.jarvis-on-pre-release</code></td>
    </td>
    <td><i>alpha</i></td>
    <td><code>.jarvis-on-alpha-release</code></td>
  </tr>
  <tr>
    <td><i>beta</i></td>
    <td><code>.jarvis-on-beta-release</code></td>
  </tr>
  <tr>
    <td><i>rc</i></td>
    <td><code>.jarvis-on-rc-release</code></td>
  </tr>
  <tr>
    <td rowspan="1">
      <code>d</code>
    </td>
    <td rowspan="1">
      <i>dev</i>
    </td>
    <td rowspan="1">
      <code>.jarvis-on-dev-release</code></td>
    </td>
    <td><i>temp</i></td>
    <td><code>.jarvis-on-temp-release</code></td>
  </tr>
</table>

The following environment variables are available in the templates:

* `JARVIS_RELEASE_TIER` - the **tier** that the current release belongs to (`stable`, `pre` or `dev`)
* `JARVIS_RELEASE_TYPE` - the **type** of the current release (e.g. `main`, or `alpha`)

Composite templates are also available for convenience:

* `.jarvis-on-release` - triggered on _any release_
* `.jarvis-on-stable-or-pre-release` - triggered on _stable_ OR _pre_ releases
* `.jarvis-on-pre-or-dev-release` - triggered on _pre_ OR _dev_ releases

&nbsp;

## Release Templates

> ⚠️ To use release templates you need to enable the Gitlab CI deploy key *with write access* for your repository in GitLab. You can find it under your repository's settings: `Settings > Repository > Deploy Keys`. You also need to add the *GitLab CI* user as a project member with at least *Reporter* role.

### `.jarvis-main-release`

**Release Tier:** _stable_

The `.jarvis-main-release` job template can be used to implement a conventional commits based release workflow. It uses the [standard version](https://github.com/conventional-changelog/standard-version) npm package to automatically calculate your next [semantic version](https://semver.org) based on your  [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/), and generate a nice changelog.

When you trigger this job, it is going to check the commits between `HEAD` and your previous release, then write a new version number to `package.json`, update your `CHANGELOG.md`, and create and push a new tag with the new version number. This tag should be used to trigger your build/deploy pipeline.

Issue links generated into `CHANGELOG.md` can be customized with the `ISSUE_PREFIXES` and `ISSUE_URL_FORMAT` variables. By default these are configured for GitLab issues. For Jira, you would override `ISSUE_PREFIXES` to `EXAMPLE-` and `ISSUE_URL_FORMAT` to `https://yourcompany.atlassian.net/browse/{{prefix}}{{id}}` either in your `.gitlab-ci.yml` or in GitLab CI/CD Variables.

#### Manual Versioning

The template `.jarvis-main-release` has an option to manually specify a version to release by setting the environment variable `RELEASE_AS` to a valid semver version number. This feature can be useful for example on a first go-live, when the version number should be manually bumped to 1.0.0

There is also a way to enforce setting the `RELEASE_AS` variable on every main release (manual versioning scheme) by including the following configuration in your pipeline:

```yaml
.jarvis-config:
  release:
    versioning: manual
```

#### Main Release From Side Branches

If the project workflow requires it, you can tweak JARVIS to allow main releases from branches other than the default. In order to achieve this use the following syntax in your `gitlab-ci.yml` file:

```yaml
...

main-release:
  extends:
    - .jarvis-main-release
    - .jarvis-release-from-any-branch
  stage: release
  ...

pre-release:
  extends: .jarvis-pre-release
  stage: release
  ...

...
```

### `.jarvis-hotfix-release`

**Release Tier:** _stable_

If you would like to patch a previous, already released code, you can create a so called _hotfix release_, using the `.jarvis-hotfix-release` job template. The job can only be triggered from a branch with the naming convention `hotfix/vX.Y.Z` where `X.Y.Z` is the _main release_ wanted to be patched.

This template behaves similarly to `.jarvis-main-release` meaning:

1. it updates the version number in the `package.json`
2. calculates the changelog, and pushes the changes onto the branch
3. creates a tag and a release on Gitlab.

However, being a _hotfix release_, it **does NOT** calculate a semantic version, but looks up the previous _main release_ version, and adds an incrementing `-hotfix.N` suffix indicating the number of hotfix releases (the numbering starting from 1).

For example: issuing the _first_ hotfix for version `1.0.5` would result in `1.0.5-hotfix.1` the _second_ one `1.0.5-hotfix.2` and so on.

### `.jarvis-pre-release`

**Release Tier:** _pre_

Pre-releases can be utilised to permanently mark certain points of the codebase without bumping semantic version numbers. Pre-releases can be issued as `alpha`, `beta` or `rc` release types. There's no hierarchy enforced between the types, you can use them according to your own workflow (e.g. start releasing `alpha` versions at the beginning of development, `beta` versions during internal testing and `rc` versions during the User Acceptance Testing period)

The job can only be triggered from a **release branch** branch with the naming convention `release/vX.Y.Z` where `X.Y.Z` is the version of the release currently under development.

The template behaves similarly to `.jarvis-hotfix-release` as it **does NOT** calculate a new semantic version, but takes the `X.Y.Z` release version from the branch name and adds a suffix related to the release **type** with an increasing serial number suffix, e.g. `2.0.0-alpha.1` then `2.0.0-alpha.2` then `2.0.0-beta.1` and so on.

### `.jarvis-temp-release`

**Release Tier:** _dev_

If you would like to create **temporary** releases (even from side branches) between stable releases then you can use the `.jarvis-temp-release` job template. This job won't increment the version number stored in your `package.json` and  won't create any commits on your branch. It will only create and push a new tag based on your branch name. This tag can then be used to trigger a build/deploy pipeline.

> ❗ It is **NOT RECOMMENDED** to use `temp` releases in any production-like environment. You can utilise these releases for experimentation, but it is advised to create a `stable` or `pre` release from your changes.

By default `.jarvis-temp-release` also does not create [Releases](https://docs.gitlab.com/ee/user/project/releases/) on GitLab. If you wish to enable this (mainly for testing purposes) you can set the `CREATE_GITLAB_PRE_RELEASE` variable to `true` on your `.jarvis-pre-release` job.

&nbsp;

## Linter Templates

### `.jarvis-commitlint`

The `.jarvis-commitlint` job template provides a quick way to check your project's merge requests whether its commits follow [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) standards.

```yaml
commitlint:
  stage: lint
  extends: .jarvis-commitlint
```

#### Author and committer email linting

The template also offers _linting the author and commiter email addresses_ of commits against a whitelist. The **recommended way** is to define `JARVIS_COMMITLINT_DOMAIN_WHITELIST` as a project/group/instance level variable as a `|` (pipe) separated list of email domains like `example.com|foobar.com`.

Alternatively you can override the `DOMAIN_WHITELIST` variable for your project-specific "commitlint" job instance using:

```yaml
commitlint:
  stage: lint
  extends: .jarvis-commitlint
  variables:
    DOMAIN_WHITELIST: example.com|foobar.com
```

### `.jarvis-docker-lint`

The `.jarvis-docker-lint` provides a convenient job template to lint Dockerfiles using [Hadolint](https://github.com/hadolint/hadolint). The job is only istantiated on `Dockerfile` changes.

To specify a `Dockerfile` other than the one in the repository root you can override the `DOCKER_FILE_DIR` and `DOCKER_FILE` variables!

Example use for a simple repo:

```yaml
docker-lint:
  stage: lint
  extends: .jarvis-docker-lint
```

> ⚠️ Please note that due to a bug in MR change detection if you override the DOCKER_FILE_DIR and DOCKER_FILE variables you need to adjust the directive

```yaml
docker-lint:
  stage: lint
  extends: .jarvis-docker-lint
  # optional, if Dockerfile is not located in the repository root
  variables:
    DOCKER_FILE_DIR: docker
    DOCKER_FILE: my_docker_file
  # do not forget to change the trigger
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - docker/my_docker_file
```

### `.jarvis-parallel-docker-lint`

You can also lint multiple `Dockerfiles` in like with [Parallel jobs](#parallel-jobs) in this case use the following syntax. In this case you should **NOT** override `DOCKER_FILE_DIR` as that is dynamically pointed to the right project folder for each parallel job.

```yaml
dockerlint:
  extends: .jarvis-parallel-docker-lint
  stage: lint
```

### `.jarvis-lockfile-lint`

[lockfile-lint](https://www.npmjs.com/package/lockfile-lint) is a tool for node-based projects that helps ensure that package lockfiles are secure. The tool checks for schemes, hosts, urls and even integrity. [Why npm lockfiles can be a security blindspot for injecting malicious modules?](https://snyk.io/blog/why-npm-lockfiles-can-be-a-security-blindspot-for-injecting-malicious-modules/).

```yaml
lockfile-lint:
  stage: lint
  extends: .jarvis-lockfile-lint
```

By default, lockfile-lint will look for a configuration file named `.lockfile-lintrc` in the project directory. Here's an example of a `.lockfile-lintrc` file:

```
{
  "path": "package-lock.json",
  "allowedHosts": ["npm", "gitlab.supercharge.io"],
  "allowedSchemes": ["https:"]
}
```

#### Configuration

The `.lockfile-lintrc` file can be customized to suit the needs of your project. Here's a breakdown of the available options:

- **`path`**: The path to the lockfile. This is typically `"package-lock.json"` for npm projects or `"yarn.lock"` for yarn projects. This option is **required**.
- **`type`**: The type of package manager used. This should be either `"npm"` or `"yarn"`.
- **`format`**: Sets what type of report output is desired, one of `"pretty"` or `"plain"`. If `"plain"` is selected, colors and status symbols will be removed from the output. This option defaults to `"pretty"`.
- **`validateHttps`**: Validates the use of HTTPS as protocol schema for all resources in the lockfile. This option cannot be used together with `allowedSchemes`.
- **`allowedSchemes`**: A list of URL schemes that are allowed in the lockfile such as "https:", "http", "git+ssh:", or "git+https:". This can help prevent insecure URLs from being included in the lockfile. By default, lockfile-lint allows https: URLs. This option cannot be used together with `validateHttps`.
- **`allowedHosts`**: A list of hosts that are allowed in the lockfile. This can help prevent malicious packages from being installed. By default, lockfile-lint allows packages from npm and yarnpkg.com.
- **`allowedUrls`**: Allowed URLs (e.g. https://github.com/some-org/some-repo#some-hash).
- **`emptyHostname`**: Allow empty hostnames, or set to false if you wish for a stricter policy. Values are `true` or `false`. Default value is `true`.
- **`validatePackageNames`**: Validates that the resolved URL matches the package name. Values are `true` or `false`.
- **`validateIntegrity`**: Validates the integrity field is a sha512 hash. Values are `true` or `false`.

### `.jarvis-renovate-lint`

Project-level configuration for [Renovate](#renovate-templates) can be linted using the supplied `.jarvis-renovate-lint` template.

```yaml
renovate-lint:
  stage: lint
  extends: .jarvis-renovate-lint
```

By default the template runs for Merge Requests if a change is detected in any of the [supported configuration files](https://docs.renovatebot.com/configuration-options/) except when the config is embedded in `package.json` (which is not recommended to do). If a custom file need to be linted, define the `CONFIG_FILE` variable in your job.

&nbsp;

## Parallel Templates

### `.jarvis-parallel`

JARVIS provides support for running parallel jobs to build projects simulatenously in a monorepo-like setup. The basic `.jarvis-parallel` template simplifies the [parallel:matrix](https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix) syntax the following way:

```yaml
.jarvis-config:
  parallel:
    projects: [app1, app2]

build:
  extends: .jarvis-parallel
  stage: build
  script:
    - ./gradlew build $PROJECT
```

The example above will result in two parallel jobs `build:app1`, `build:app2` with the `$PROJECT` variable set respectively in each job and the `$PROJECTS_PATH` variable set to `src/projects` in both of them.

If you need to introduce another dimension besides the projects you can do so by overriding the original `parallel:matrix` syntax in the following way:

```yaml
.jarvis-config:
  parallel:
    projects-path: src/projects
    projects: [app1, app2]

build:
  extends: .jarvis-parallel
  stage: build
  script:
    - ./gradlew build $PROJECT $FLAVOR
  parallel:
    matrix:
      - PROJECTS: !reference [.jarvis-config, parallel, projects]
        FLAVOR: [debug, prod]
```

The example above will result in 4 created jobs `build:app1:debug`, `build:app1:prod`, `build:app2:debug` and `build:app2:prod`.

> ✅ The real power of parallel jobs comes with the `.jarvis-parallel-on-demand` template for merge requests, make sure to check it out at the [respective section](#parallel-on-demand-jobs)

### `.jarvis-parallel-on-demand`

By using [Parallel jobs](#parallel-jobs) under the hood you can easily optimize your monorepo _Merge Request_ process by extending the `.jarvis-parallel-on-demand` template.

First of all make sure your source code is organized in a way, that the projects that make up your monorepo are situated in a **single parent directory**.

Let's have the following folder structure to work with:

```
.
├── src/
│   ├── projects/
│   │   ├── app1/
│   │   │   └── ...
│   │   └── app2/
│   │       └── ...
│   ├── common/
│   │   └── ...
│   └── ...
├── e2e-tests/
│   └── ...
├── gitlab-ci.yml
└── ...
```

The second step is to set up your pipeline with JARVIS according to the folder structure above. Please note, that you need to define folders containing common code, where if changes are detected, it need to trigger **every** parallel job to avoid breaking pipelines.

> ⚠️ Paths for common code, defined in the `common-paths` paramter **MUST NOT** include any "project" directory, even in recursive definitions in order to change detection work propery. E.g simply defining `['**/*']` as `commmon-paths` won't work.

```yaml
.jarvis-config:
  parallel:
    common-paths: ['*', 'src/common/**/*', 'e2e-tests/**/*']
    projects-path: src/projects
    projects: [app1, app2]

build:
  extends: .jarvis-parallel-on-demand
  stage: build
  script:
    - ./gradlew build $PROJECT
```

What happens if you use `.jarvis-parallel-on-demand` in a **Merge Request** job:

* if any change is detected in the **common paths** ➡️ both `build:app1` and `build:app2` jobs will run
* if changes are **only in** `src/projects/app1` ➡️ only `build:app1` will run
* if changes are **only in** `src/projects/app2` ➡️ only `build:app2` will run

> ⚠️ Please note that `.jarvis-parallel-on-demand` should and can only be used in **Merge Request** pipelines, detecting changes won't work for any other type.

&nbsp;

## Docker Templates

### `.jarvis-docker-build`

Running Docker builds in a containerized environment (e.g in a Kubernetes cluster) might be tricky because userspace restrictions. Project [kaniko](https://github.com/GoogleContainerTools/kaniko) aims to solve it by providing a way of building container images in environments that can't easily or securely run a Docker daemon, such as a standard Kubernetes cluster.

The template `.jarvis-docker-build` can be used to trigger Docker image build using kaniko. The default configuration is very simple for the following (most frequent) case:

* Docker build context is the repository root
* there is a `Dockerfile` in the repository root
* the build artifact should go tho the default [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/) associated with the project

```yaml
check-build:
  extends:
    - .jarvis-on-release
    - .jarvis-docker-build
    - .jarvis-pod-size-l
  stage: build
  tags:
    - amd64
```

The following table summarizes the environment variables that can be used to customize the build process:

| Variable | Default Value | Comment |
|----------|---------------|---------|
| `MODE` | `build-and-push` | values can be: `build-and-push` and `build` |
| `EXTRA_OPTIONS` | `''` | can be used to add extra options to the `/kaniko/executor` command, e.g `--build-args`|
| `CONTEXT` | `.` | Docker build context, defaults to repo root |
| `DOCKERFILE` | `Dockerfile` | path of the Dockerfile |
| `DESTINATION` | `$CI_REGISTRY_IMAGE` | name of the image to be built, defaults to GitLab CI config |
| `VERSION` | `''` | if not explicitly set: semantic version for releases, short commit hash for everything else |
| `DOCKER_HUB_USERNAME` | `$JARVIS_DOCKER_HUB_USERNAME` | DockerHub auth username, defaults to global JARVIS config variable |
| `DOCKER_HUB_PASSWORD` | `$JARVIS_DOCKER_HUB_PASSWORD` | DockerHub auth password |
| `PRIVATE_REGISTRY` | `$CI_REGISTRY` | private registry URL, defaults to _GitLab Container Registry_ |
| `PRIVATE_REGISTRY_USERNAME` | `$CI_REGISTRY_USER` | private registry username |
| `PRIVATE_REGISTRY_PASSWORD` | `$CI_REGISTRY_PASSWORD` | private registry password |

A more complex example for overriding options in an MR pipeline:

```yaml
docker-build:
  extends:
    - .jarvis-on-merge-request
    - .jarvis-docker-build
    - .jarvis-pod-size-l
  stage: check
  tags:
    - amd64
  variables:
    MODE: build   # build-only mode for MR
    EXTRA_OPTIONS: "--build-arg TOKEN=$CI_JOB_TOKEN"   # pass extra arguments
```

### `.jarvis-parallel-docker-build`

If you want to run Docker build in [parallel](#parallel-templates), you can do so by using the `.jarvis-parallel-docker-build` template. It essentially extends a `.jarvis-parallel` pipeline with the default project configuration and modifies the following environment variables:

| Variable | Default Value | Comment |
|----------|---------------|---------|
| `DOCKERFILE` | `$PROJECT_DIR/Dockerfile` | each project has its own Dockerfile |
| `DESTINATION` | `$CI_REGISTRY_IMAGE/$PROJECT` | build one image per project |

So the default configuration fits when:

* the Docker build context is still the repository root
* `Dockerfiles` are located in each project folder root

&nbsp;

## Renovate Templates

With JARVIS you can easily use [Renovate](http://renovatebot.com/) to update your project dependencies. The _"Renovate Bot"_ opens MRs according to your pipeline configuration with any configured dependency update and also maintains a handy "Dependency Dashboard" to have extra visibility on the project's dependency status.

The _Renovate_ JARVIS integration comes with really simple configuration and flexible project-level customization options which can be read below.

### Bot Configuration

First in order to use the Renovate integration you need to create a _"Renovate Bot"_ on GitLab which has access to every repository that is configured to run on with at least **"Developer"** permissions, as the bot is creating branches and Merge Requets during its operation.

Once the _"Renovate Bot"_ user is created a [GitLab Personal Access Token](https://gitlab.supercharge.io/-/profile/personal_access_tokens) needed to be generated for the user with the following scopes:

* `read_user`
* `api`
* `write_repository`
* `read_registry`

As _Renovate_ utilizes the github.com API to parse changelog a [GitHub Personal Access Token (Classic)](https://github.com/settings/tokens) need to be created with the following permissions:

* `public_repo`

| variable                            | required | description                                                         |
| ----------------------------------- | -------- | ------------------------------------------------------------------- |
| `JARVIS_RENOVATE_BOT_USER_NAME`     | \*       | User name for commits (e.g Renovate Bot)                            |
| `JARVIS_RENOVATE_BOT_USER_EMAIL`    | \*       | Email address for commits (e.g renovatebot@yourdomain.com)          |
| `JARVIS_RENOVATE_BOT_TOKEN`         | \*       | GitLab Personal Access Token of the "Renovate Bot" user (see above) |
| `JARVIS_RENOVATE_GITHUB_COM_TOKEN`  | \*       | GitHub Personal Access Token (read-only for changelog parsing)      |

> ⚠️ Same as with the general config options, these variables above should be created as global variables (in a self-managed GitLab instance) or group-level variables (on GitLab.com) to avoid unnecessary configuration steps for new projects.

### Renovate Presets

JARVIS is utilising another open-source project called [JARVIS Renovate Presets](https://gitlab.com/team-supercharge/jarvis/renovate-presets) to store the base preset configuration for

* all projects using JARVIS - `base` preset
* and additionally platform-specific configurations - e.g. `api-generator` preset

### `.jarvis-renovate`

Including _Renovate_ in the project pipeline is quite simple, and can be added with the following minimal configuration in `.gitlab-ci.yml`

```yaml
...

stages:
  - release
  - build
  - renovate                   # add a separate stage

...

renovate:
  extends: .jarvis-renovate    # extend from template
  stage: renovate

...
```

Once the job is defined in the pipeline configuration, initialize the project-level _Renovate_ configuration in `renovate.json` or in [any other](https://docs.renovatebot.com/configuration-options/) supported paths:

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "gitlab>team-supercharge/jarvis/renovate-presets//base#<VERSION>",
  ]
}
```

For the latest version number and additional available presets please refer to the [JARVIS Renovate Presets](https://gitlab.com/team-supercharge/jarvis/renovate-presets) sub-project. Don't worry, this versions will be also auto-updated by extending from the `base` preset above.

Finally the configured _Renovate_ job can be then run by:

* creating a [pipeline schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#add-a-pipeline-schedule) to run the check on a specified interval e.g daily
* triggering a pipeline from the web using _"Build -> Pipelines -> Run Pipeline"_

> ⚠️ In both cases **the `RUN_RENOVATE=true` variable need to be set** in order to prevent clashes with other schedules or web-triggered jobs (also don't forget to enable [liting](#jarvis-renovate-lint) the configuration)

&nbsp;

## Notification Templates

JARVIS is utilising a sub-project called [JARVIS Slack Notifier](https://gitlab.com/team-supercharge/jarvis/slack-notifier) to introduce nicely formatted release notifications percisely marking the beginning and the end of a build - incuding success and failure statuses.

### Usage

The following example contains the available build lifecycle templates `.jarvis-notify-build-start`, `.jarvis-notify-build-success` and `.jarvis-notify-build-failed`.

```yaml
stages:
  - build
  - notify

# build stage
notify-build-start:
  extends: .jarvis-notify-build-start
  stage: build

build:
  extends: .jarvis-on-release
  stage: build
  tags:
    - m
  script:
    - npm run build

# notify stage
notify-build-success:
  extends: .jarvis-notify-build-success
  stage: notify

notify-build-failed:
  extends: .jarvis-notify-build-failed
  stage: notify
```

**⚠️ Scheduling considerations:**

* The `notify-build-start` job **does NOT** require to be placed in a previous stage or a a `needs` dependency for the `build` job not to delay the build start unnecessarily
* The `notify-build-success` and `notify-build-failed` jobs can be conveniently placed in **separate subsequent stage** so that:
  - when the **Build** phase of the pipeline is split into multiple stages, the result will be reported correctly
  - with the `notify-build-start` job being in a previous stage, the "result" notification will never get triggered before the "start" one, not even if the latter is delayed for some reason

_Additional notes:_

* Every notification template is marked with `tags: [s]` to use the smallest possible worker nodes
* By default notifications are only sent for **main releases**, if you want to enable these for pre-releases as well, please
  - enable the `CREATE_GITLAB_PRE_RELEASE` flag on your [.jarvis-pre-release](#jarvis-pre-release) job
  - override the default extends statement with the following:

```yaml
extends:
  - .jarvis-notify-build-{start|success|failed}
  - .jarvis-on-release
```

### `.jarvis-send-message`

In some cases (eg. test run results) you might want to send a custom message to Slack using [JARVIS Slack Notifier](https://gitlab.com/team-supercharge/jarvis/slack-notifier) defined by your project needs. The `.jarvis-send-message` template makes it convenient. In the following example you can see how it can be used to send a message when the Merge Request Phase runs.

```yaml
send-message:
  extends:
    - .jarvis-send-message
    - .jarvis-on-merge-request
  stage: check
  variables:
    MESSAGE_TEXT: 'Hello from JARVIS! :wave:'
    BLOCKS_PATH: ./misc/message-blocks.json
    ENV_PATH: ./.jsn_env
```

The required `MESSAGE_TEXT` variable is used to define the plain message text. The optional `BLOCKS_PATH` variable is used to define the path to a JSON file containing the Slack Block Kit message blocks (in this case the mandatory message is displayed in e.g push notifications). The optional `ENV_PATH` variable is used to define the path to a file containing the environment variables to be used in the defined blocks, if any. For more information, please refer to the [JARVIS Slack Notifier](https://gitlab.com/team-supercharge/jarvis/slack-notifier) project.

&nbsp;

## Node.js Templates

### `.jarvis-node-preheat-cache`

In order to configure caching the `.jarvis-node-preheat-cache` template needs to be extended. Use the following configuration in your pipeline:

```yaml
# default cache config from JARVIS
cache: !reference [.jarvis-node-cache]

default:
  image: node:14.17.5 # use your own node image
  tags:
    - arm64 # select a default architecture

stages:
  - preheat
  - check

# preheat cache for MRs
preheat:
  extends:
    - .jarvis-on-merge-request
    - .jarvis-node-preheat-cache
  stage: preheat

# run checks on MR
lint:
  extends:
    - .jarvis-on-merge-request
  stage: check
  script:
    # !!! no npm ci needed !!!
    - npm run lint

build:
  extends:
    - .jarvis-on-merge-request
  stage: check
  script:
    # !!! no npm ci needed !!!
    - npm run build
```

The resulting `preheat` job in the pipeline will do the following if the cache for the current state of `package-lock.json` is **not hot yet**:

* install dependencies using the default `npm ci` command (you can override what is being run with the `$COMMAND` variable)
* the job _purposefully fails_ after preheating the cache and uploads resulting cache files
* restarts itself automatically after the failure

If the cache **is already hot**, or after going to the previous steps the job just _immediately returns_ with success - and this is where the optimization comes from.

If the cache is not created successfully, then it will remove the `node_modules` folder, so the next run will realize that the cache is not created due to script `$COMMAND` failure.

> ⚠️ Please note that the example above only preheats the cache for merge request jobs, If needed please add another "preheat" job to your release pipelines as well:

```yaml
# add JARVIS release jobs and stages

preheat-release:
  extends:
    - .jarvis-on-release # notice this
    - .jarvis-node-preheat-cache
  stage: pre-build

build:
  extends:
    - .jarvis-on-release
  stage: build
  script:
    # !!! no npm ci needed !!!
    - npm run build
```

Please note, that the version number increases on every new release which affects the `package-lock.json` files - the cache key, so this results in a cache miss on every new release. Therefore creating a separate "preheat" job for `.jarvis-on-release` is only useful if there are multiple jobs in the Build phase which could reuse the cache between themselves.

#### Technical details

Nailing down the optional Node.js cache configuration is not an easy task using the cache features of GitLab CI. Although a properly configured project could easily depend on the **content** of the `package-lock.json` file: create a cache of dependencies whenever `package-lock.json` changes, and **do not even schedule** jobs to do `npm ci` or `npm install` any more in the future.

In theory the _"ultimate"_ preheat job configuration would combine the approach above with change detection and look like this:

```yaml
# !!! DO NOT USE THIS !!!

preheat:
  # cache the node_modules/ directory
  # based on the contents of package-lock.json
  cache:
    key:
      files:
        - package-lock.json
    paths:
      - node_modules/
    expires: never
  # if the job even starts, run npm ci
  # to initialize node_modules
  script:
    - npm ci
  # do not even schedule a new 'preheat' job if cache
  # is already present (lock file did not change)
  only:
    changes:
      - package-lock.json

# !!! DO NOT USE THIS !!!
```

However this won't work reliably because of clashing in the following two points:

1. `only:changes` and `rules:changes` only checks if the changeset of a push or a merge request lists the file as changed - this can result in missing cache entries e.g on rebasing commits
2. for `cache:key:files` the cache key is not calculated from the **actual contents** of the file, but for performance reasons the calculation uses the **last commit SHA** that affected the file - when changing commits (e.g rebasing or force pushing branches) this would result in unnecessary cache misses

&nbsp;

## Image Templates

* `.jarvis-deploy` - uses a base image that contains everything to execute a deployment on k8s (nodejs, curl, ssh, gnupg, kubectl, helm, aws cli, eksctl, terraform)
* `.jarvis-docker-cli` - uses a base image that contains just a plain simple docker-cli
* `.jarvis-slack-notifier` - uses a Node-based image containing with [JARVIS Slack Notifier](https://gitlab.com/team-supercharge/jarvis/slack-notifier) preinstalled in it, usable with the `jsn` binary

### Helper Images

The helper images used by JARVIS are managed in their [own repository](https://gitlab.com/team-supercharge/jarvis/images). To consider performance during job runs please add static steps to the respecitve base images and bump image version in JARVIS.

> ⚠️ Please *DO NOT* use the images directly, only by referencing their respective job templates! ⚠️

### Custom Versions

Some Image Templates support setting their version to be used (either higher or lower than the default bundled with JARVIS) by overriding environment variables.

| template | variable |
|-|-|
| `.jarvis-slack-notifier` | `JARVIS_SLACK_NOTIFIER_VERSION` |

> ⚠️ Please make note that the "default" version of such images will be continuously updated with subsequent JARVIS releases. If you opt-out from using the JARVIS-bundled versions, make sure you keep your versions up to date with every new JARVIS release to avoid unintended breaking changes.

&nbsp;

## Util Templates

### `.jarvis-jacoco-to-cobertura`

GitLab supports [visualizing code coverage data](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html) by parsing [Cobertura](https://cobertura.github.io/cobertura/) reports. Since Cobertura is no longer maintained this job template helps converting Jacoco XML coverage reports to the Cobertura format. The [GitLab recommended way](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html#gradle-example) to do this is actually really error-prone and does not support most repository layouts.

#### Usage

Run your tests and generate an XML jacoco report (eg. [using gradle](https://docs.gradle.org/current/userguide/jacoco_report_aggregation_plugin.html#jacoco_report_aggregation_plugin)). Archive the report coverage XML as an artifact.
Extend this template (`.jarvis-jacoco-to-cobertura`) and set the coverage XML location in the `JACOCO_XML_NAME` variable.

```yaml
convert-coverage-data:
  extends: .jarvis-jacoco-to-cobertura
  stage: convert-coverage-data
  variables:
    JACOCO_XML_NAME: "jacoco-report.xml"
```

&nbsp;

## Script Templates

In order to use **Script Templates** you need to use [`!reference` tags](https://docs.gitlab.com/ee/ci/yaml/#reference-tags) to include the desired functionality (see examples below)

### Extracting Release Metadata

In jobs extending the `.jarvis-on-XXX-release` templates (in these jobs the last commit is guaranteed to be a release commit) there is a way to easily extract release information using `!reference [.jarvis-extract-release-*]`. The extracted information can be conveniently used in release/deploy scripts, notifications, etc.

> ⚠️ These scripts need the `git` binary to be available.

The current version information is available both written in transient files and as environment variables:

* `.jarvis-extract-release-version` - `VERSION` file and `$VERSION` env var - the current version being built
* `.jarvis-extract-release-changes` - `CHANGES.md` file and `$CHANGES` env var - extracted content of `CHANGELOG` for the current version
* `.jarvis-extract-release-changes-since-last-deployment` - `CHANGES_SINCE_LAST_DEPLOYMENT.md` file and `CHANGES_SINCE_LAST_DEPLOYMENT` env var - extracted content of `CHANGELOG` since the previous deployment
  - in addition, this script needs the `curl` binary and `GITLAB_API_TOKEN` env var containing a Gitlab personal/project access token with API access to the project.

Usage:

```yaml
build-job:
  extends: .jarvis-on-release
  script:
    - !reference [.jarvis-extract-release-version]
    - cat VERSION
    - echo $VERSION
    - !reference [.jarvis-extract-release-changes]
    - cat CHANGES.md
    - echo "$CHANGES" # use double quotes to preserve newlines
```

### `.jarvis-publish-generic-package`

This script simplifies dealing with the HTTP request which is needed to publish a [generic package](https://docs.gitlab.com/ee/user/packages/generic_packages/) on GitLab for the current project.

> ⚠️ This script needs the `curl` and `git` binaries to be available.

The following environment variables need to be defined before `!reference`-ing the script template. Note: the script always populates the _artifact version_ with the value from `.jarvis-extract-release-version`.

| variable | description |
|-|-|
| `SOURCE_FILE_PATH` | local path for a file to upload |
| `PACKAGE_NAME` | defines `package_name` as described on the [GitLab API](https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file) |
| `TARGET_FILE_NAME` | defines `file_name` as described on the [GitLab API](https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file) |

Usage:

```yaml
publish-binary:
  extends: .jarvis-on-release
  variables:
    SOURCE_FILE_PATH: out/bin/app.zip
    PACKAGE_NAME: app-binary
    TARGET_FILE_NAME: app.zip
  script:
    - !reference [.jarvis-publish-generic-package]
```

### `.jarvis-download-generic-package`

Like its counterpart `.jarvis-publish-generic-package` this template is for downloading files published in a [generic package](https://docs.gitlab.com/ee/user/packages/generic_packages/) on GitLab.

> ⚠️ This script needs the `wget` and `git` binaries to be available.

The following environment variables need to be defined before `!reference`-ing the script template. Note: the script always populates the _artifact version_ with the value from `.jarvis-extract-release-version`.

| variable | description |
|-|-|
| `PACKAGE_NAME` | defines `package_name` as described on the [GitLab API](https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file) |
| `FILE_NAME` | defines `file_name` as described on the [GitLab API](https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file) |

Usage:

```yaml
publish-binary:
  extends: .jarvis-on-release
  variables:
    PACKAGE_NAME: app-binary
    TARGET_FILE: app.zip
  script:
    - !reference [.jarvis-download-generic-package]
```

&nbsp;

# Migration Guide

## From `10.x.x` to `11.0.0`

This version introduces several breaking changes. If you are interested in the details, read below.

**TL;DR**

1. if you have extended `.jarvis-pre-release` keep it (as it will provide the new "alpha/beta/rc" functionality) but also add a job extending `.jarvis-temp-release`
2. if you have used the `JARVIS_MAIN_RELEASE`, `JARVIS_PRE_RELEASE` or `JARVIS_HOTFIX_RELEASE` variables migrate to the new `JARVIS_RELEASE_TIER` and/or `JARVIS_RELEASE_TYPE` variables
3. if you have jobs extending `.jarvis-on-main-release` change them to `.jarvis-on-stable-release` to keep them triggered for _"hotfix releases"_

### _"pre releases"_ became _"temp releases"_

With cleaning up release **tiers** and release **types** previous _"pre releases"_ became _"temp releases"_ to better reflect their temporary nature

* if you have been extending `.jarvis-pre-release` in your pipelines, now `.jarvis-temp-release` would offer the same functionality (can be issued from any branch, and version numbers won't get bumped in the repository)
* the version naming convention _"temp release"_ versions has also changed: previously it was `X.Y.Z-<branch-slug>-<short_hash>.0` now it became `X.Y.Z-temp-<branch-slug>-<short_hash>` with a fixed `temp-` prefix after the semantic version which allows for better filtering
* for any release types in the _"dev"_ tier (currently only the _"temp"_ release type) the Git tag prefix has changed from `v` to `d` resulting in tags like `d1.0.0-temp-my-feature-abcd1234`
  - this change was triggered from GitLab's ability to handle tag and branch protection with simple wildcards instead of regexes: by protecting tags with the `v*` wildcard would result in protecting all releases

> 💡 Please note that _"pre releases"_ are also re-introduced, but they offer different functionality: to provide "alpha", "beta" and "rc" releases from dedicated release branches.

### `.jarvis-on-main-release` changes

For similar reasons (cleaning up the tiers and types) the template `.jarvis-on-main-release` will not be triggered for _"hotfix releases"_.

To have some job getting triggered for both _"main"_ AND _"hotfix"_ releases use the newly introduced `.jarvis-on-stable-release` template.

### Environment variables on releases

The `JARVIS_XXX_RELEASE` variables that has been set to the `active` string for various release types (e.g. `JARVIS_MAIN_RELEASE=active`) has been removed.

Use the newly refactored `JARVIS_RELEASE_TIER` and `JARVIS_RELEASE_TYPE` variables in scripts if you need to apply conditional logic.

## From `9.x.x` to `10.0.0`

* The breaking change affects pipelines that have been using the `.jarvis-oasg` template, so **API code generation** projects are exclusively the ones being affected. From version 10.0.0 the _OASg_ base image should be [added separately](https://gitlab.com/team-supercharge/oasg#use-with-docker) in these pipelines.

Take a typical setup before 10.0.0:

```yaml
swaggerlint:
  extends:
    - .jarvis-pod-size-s
    - .jarvis-on-merge-request
    - .jarvis-oasg
  stage: check
  script:
    - oasg lint

build-check-code:
  extends:
    - .jarvis-pod-size-m
    - .jarvis-on-merge-request
    - .jarvis-oasg
  stage: check
  script:
    - oasg generate
```

Should be using the _OASg_ image explicitly with defining a version number:

```yaml
# define local template with version number
.oasg:
  image: registry.gitlab.com/team-supercharge/oasg:<version>
  tags: [arm64]

swaggerlint:
  extends:
    - .jarvis-pod-size-s
    - .jarvis-on-merge-request
    - .oasg                      # extend local template
  stage: check
  script:
    - oasg lint

build-check-code:
  extends:
    - .jarvis-pod-size-m
    - .jarvis-on-merge-request
    - .oasg                      # extend local template
stage: check
  script:
    - oasg generate
```

**Background**
* The feature development and bugfixing of _OASg_ and _JARVIS_ is handled independently from each other, therefore it does not make sense to maintain an unnecessary coupling between the two project releases
* Relying on having a default version of _OASg_ shipped with each _JARVIS_ release would:
  - require a new _JARVIS_ release after each _OASg_ release to bump its version
  - therefore hide that fact the two projects are developed separately and kinda force the end-users to always update _JARVIS_ and _OASg_ together
  - which for most users would end up in maintaining `JARVIS_OASG_VERSION` manually defeating the purpose of the coupling even more

## From `8.x.x` to `9.0.0`

* The breaking change only affects pipelines that have been using the `.jarvis-api-generator` template. The template has been discontinued in favor of `.jarvis-oasg` for _OASg_ versions 8.0.0 and higher. Please refer to [_OASg_ Migration Guide](https://gitlab.com/team-supercharge/oasg#from-7xx-to-800) for more information about supporting Spring Boot 2 artifacts and using Java 11 if needed

## From `7.x.x` to `8.0.0`

* Existing resource allocation tags (`s`, `m`, `l`, `xl`) must not be used, you should extend your job from the .jarvis-pod-size-s, .jarvis-pod-size-m, .jarvis-pod-size-l or .jarvis-pod-size-xl templates instead.
* Since existing jobs (mostly) use amd64 based runners all jobs should be tagged with amd64 (or arm64).

## From `6.3.0` to `7.0.0`

* the [Parallel on-demand jobs](#parallel-on-demand-jobs) feature, which was introduced in 6.3.0 was not detecting changes correctly
  - if you use this feature, you must define `.jarvis-config.parallel.common-paths` in your pipeline according to the documentation above

## From `6.x.x` to `6.3.0`

Version `6.3.0` brings a couple of new features which worth the migration, although it is _optional_:

* with the [General MR template](#general-mr-template) feature introduced you may change all your jobs _running on MRs_ to have a consistent pipeline and leverage [interruptible: true](https://docs.gitlab.com/ee/ci/yaml/#interruptible) on jobs thus **reduce load on the CI**
  - use `extends: .jarvis-on-merge-request` in the job definition
  - remove `only: merge_requests` or `rules: ...` entries (follow the guide above if you have complex rules)
* if your repo is in a monorepo setup (hosting multiple distinct apps or projects) you should consider the migration to using [Parallel on-demand jobs](#parallel-on-demand-jobs) which can drastically speed up MR pipelines and also highly **reduce load on the CI**

## From `5.0.0` to `6.0.0`

* with version `6.0.0` JARVIS became an open-source project 🎉
* this means you have to update your `include` references to access JARVIS from gitlab.com
* the helper Docker images are also moved under the public GitLab-hosted registry `registry.gitlab.com/team-supercharge/jarvis/images/...` so these references need to be updated as well
* as Ed25519 keys emerging the Git SSH key used by JARVIS is not anymore separated and concatenated from multiple environment variables `DEPLOY_KEY_0`, `DEPLOY_KEY_1`, etc -- instead it isbe fetched from a single env var called `JARVIS_GIT_SSH_KEY`
* an option to configure Git commit identity for releases has been added using the `JARVIS_GIT_USER_EMAIL` and `JARVIS_GIT_USER_NAME` variables

**TL;DR**
  * update your `include:` statement according to the [Including JARVIS](#including-jarvis) section
  * search for the text `registry.supercharge.io/jarvis/images` in your `.gitlab-ci.yml` file and change it to `registry.gitlab.com/team-supercharge/jarvis/images`
  * generate a new key in Ed25519 format and store it in `JARVIS_GIT_SSH_KEY` (advised to be set globally per GitLab instance)
  * add `JARVIS_GIT_USER_EMAIL` and `JARVIS_GIT_USER_NAME` variables to configure Git commit identity (advised to be set globally per GitLab instance)

## From `4.x.x` to `5.0.0`

* the main breaking change is that previous versions of JARVIS used helper Docker images for various build steps from the internal misc/jarvis-ng or jarvis/jarvis registry paths, but those lacked proper versioning
* now these helper images are refactored into `jarvis/images` versioned semantically with JARVIS itself, and will be open-sourced in the future from this repository
* to avoid old and untracked images being used we’re going to turn off the Docker registry for `misc/jarvis-ng` and `jarvis/jarvis` on Mar 26 2021
* if you did not manually use any Docker image in your jobs e.g. `jarvis/jarvis/deploy:master` => just by setting the version number to 5.0.0 you’re good to go
* if you did use one of these helper images, please point them to `jarvis/images/<YOUR_IMAGE>:1.0.0` => this tag contains the latest version from the old registries

**TL;DR** - search for `registry.supercharge.io/jarvis/jarvis` or `registry.supercharge.io/misc/jarvis-ng`  in your `.gitlab-ci.yml`
  * if you have results migrate them to `jarvis/images/<IMAGE>:1.0.0`
  * if not, you’re good with just bumping JARVIS version

## From `3.x.x` to `4.0.0`

If you extended the `.jarvis-handle-pre-release` job in your pipeline (which did an additional pre-release calculation) please delete it and simply use the `.jarvis-main-release` and `.jarvis-pre-release` jobs in their single stage.

## From `2.x.x` to `3.0.0`

The format of the pre-release tags has been changed. If you have jobs that use regular expression triggers please update them to use this regex in case of pre-releases: `/^pre-\d+\.\d+\.\d+-.+-\b[0-9a-f]{8}\.\d+$/` or extend your job from `.jarvis-on-prerelease`.

If you use the `.jarvis-on-*` job templates to implement these triggers, you have nothing to do.

* Previous pre-release tag format: `v0.0.0-branch-commthsh`
* New pre-release tag format: `pre-0.0.0-branch-commthsh.0`

&nbsp;

# Contributing

If you have a new idea don't be afraid to share it by submitting a pull request or asking in the #sc-ci Slack channel!

For open issues and improvement ideas lease visit the [Issues](https://gitlab.com/team-supercharge/jarvis/jarvis/-/issues) page.

&nbsp;

# Versioning

This library is developed using [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) and versioned with [standard version](https://github.com/conventional-changelog/standard-version).
