# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [10.4.0](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v10.3.0...v10.4.0) (2024-03-04)


### Features

* update jarvis images to 4.1.0 ([f2164a7](https://gitlab.com/team-supercharge/jarvis/jarvis/-/commit/f2164a71c53fa6142f8d2e1f65ed2b1d5e4c2434))

## [10.3.0](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v10.2.0...v10.3.0) (2023-11-15)


### Features

* allow versionrc.js and json files ([1e6efcb](https://gitlab.com/team-supercharge/jarvis/jarvis/-/commit/1e6efcbb385be4bc6472197fc4dce9137731f6db))

## [10.2.0](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v10.1.2...v10.2.0) (2023-10-30)


### Features

* change naming scheme for hotfix release versions ([574e94f](https://gitlab.com/team-supercharge/jarvis/jarvis/-/commit/574e94f9fd98c316aa98df900f2d42fd39c31a4e))
* hotfix releases should trigger .jarvis-on-main-release jobs ([5e4dffd](https://gitlab.com/team-supercharge/jarvis/jarvis/-/commit/5e4dffde88a5dc193794fd1c0b2363397e454e0a))
* more restrictive branch naming patterns from hotfix releases ([fc068fa](https://gitlab.com/team-supercharge/jarvis/jarvis/-/commit/fc068fac3e2f9f5f5bac32c4ecf2f7cbc9fdd413))


### Documentation

* complete documentation on variables available on releases ([61cf74b](https://gitlab.com/team-supercharge/jarvis/jarvis/-/commit/61cf74b82148b0dcaa7094bca4329c2d36341b0a))

### [10.1.2](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v10.1.1...v10.1.2) (2023-09-13)


### Bug Fixes

* export public env variables in .jarvis-extract-release scripts ([b042f83](https://gitlab.com/team-supercharge/jarvis/jarvis/-/commit/b042f8321b8769fe3c16f449921027126a11124e))
* fix tags are not fetched during .jarvis-extract-release-changes-since-last-deployment ([980eb07](https://gitlab.com/team-supercharge/jarvis/jarvis/-/commit/980eb07e72359754dbaf2be5339f946898030f85))


### Documentation

* add node release cache suggestions ([a707f6e](https://gitlab.com/team-supercharge/jarvis/jarvis/-/commit/a707f6e609a5efa8cde69885fb1ecd72f99549f3))

### [10.1.1](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v10.1.0...v10.1.1) (2023-07-14)


### Bug Fixes

* fix default urls for linked release resources ([43aca3c](https://gitlab.com/team-supercharge/jarvis/jarvis/-/commit/43aca3cd1f655aafa5528207f366861b2c20a60b))

## [10.1.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v10.0.0...v10.1.0) (2023-06-16)


### Features

* add Renovate templates for dependency management ([b2b834c](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/b2b834c7c0c219e2d538eb475c8453030b653cca))
* disallow release jobs from web source ([44aeca3](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/44aeca3d7b8e6022e75637c7bcc22372a4c4dae0))


### Bug Fixes

* remove CI_COMMIT_TAG workaround for custom message sending ([726460c](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/726460cb4dc31bcb039af5861a7c2b93d22dffd3))


### Documentation

* improve readability of config variable description ([2e5b489](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/2e5b489f5bae556afbc2c32237001c6db25fedf1))

## [10.0.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v9.2.0...v10.0.0) (2023-05-05)


### ⚠ BREAKING CHANGES

* remove .jarvis-oasg template in order to decouple the two projects

### Features

* remove .jarvis-oasg template in order to decouple the two projects ([d42d038](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/d42d038ce5cf232bc279b5f2897b5f81aeca758b))

## [9.2.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v9.1.0...v9.2.0) (2023-05-05)


### Features

* reduce cpu requests for big pod sizes ([c080a59](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/c080a59b6a48b023ef293ebd012ff10faa7704f1))
* update kaniko to 1.9.2 ([434a64b](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/434a64bd4f047e1b65b2fa6fae6aac3e8ddc664f))


### Bug Fixes

* update default jsn version to latest 1.2.4 ([d012c78](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/d012c7878be6f0191a649d1d8112224b8d80a279))

## [9.1.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v9.0.0...v9.1.0) (2023-02-28)


### Features

* update default oasg version to latest 9.1.0 ([0fc5acf](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/0fc5acf4f991ceab65663ce1fdbfa027c4fc074d))


### Bug Fixes

* update default jsn version to latest 1.2.2 ([fd3efa0](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/fd3efa063bae02f595dc93d82100e1eb00d48a4c))

## [9.0.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v8.0.1...v9.0.0) (2023-02-24)


### ⚠ BREAKING CHANGES

* update default oasg version to latest 9.0.0
* replace .jarvis-api-generator with .jarvis-oasg template

### Features

* add .jarvis-lockfile-lint template for linting node package locks ([7be97df](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/7be97dfb6f8b95349e14d76f9b89148ad5759a4a))
* add .jarvis-send-message template to send custom messages to Slack ([a5e647b](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/a5e647ba09559112901cca33dc26bddfe72fcfea))
* add hotfix release job template ([bf548f2](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/bf548f2fb24e379b8944b2677b05d063694e5b16))
* add script template to download files from generic packages ([c308090](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/c30809005f22e9b77eb935dba12de129a54eedc0))
* add script template to publish files as generic packages ([cce9d69](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/cce9d69b66c66b1b00ac495b63cf397cdb38cb93))
* bump slack notifier (jsn) version to 1.2.0 ([b60bed0](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/b60bed09acdfa82b8db838923950926366440afb))
* check committer email in .jarvis-commitlint ([1bd11c6](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/1bd11c6111cc4acd713ba3d685dbd6662e740ee7)), closes [#12](https://gitlab.com/team-supercharge/jarvis/jarvis/issues/12)
* replace .jarvis-api-generator with .jarvis-oasg template ([67403da](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/67403da90d30983ca47af526e71ff70628263ccc))
* update base images version to 4.0.0 ([9489451](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/94894512c20384ceab2c9ce41344dc42ccb3093b))
* update default oasg version to latest 9.0.0 ([cbb9479](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/cbb94797f3e5ee558af40a4f7802535f1bae9ccc))


### Bug Fixes

* tag templates using jarvis/images with amd64 ([7cab8ce](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/7cab8ce0d1f0ca77fbc8856ba11bd05e3448744d))
* update default jsn version to latest 1.2.1 ([2d66934](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/2d6693468592a29583d1665de0017d042ea56ef6))
* update default oasg version to latest 8.0.1 ([304cb60](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/304cb605f681feb6048252499da53572990c002d))


### Documentation

* fix typo in .jarvis-on-merge-request sample ([787d143](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/787d143cc3972e5bdb6a0fff8e6996a5a4dd47c6))

### [8.0.1](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v8.0.0...v8.0.1) (2022-11-07)


### Bug Fixes

* for projects with existing .versionrc ([ed96a00](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/ed96a00ec402e3f7211f3431e69a5a5e8b8cc6a1))

## [8.0.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v7.3.0...v8.0.0) (2022-10-25)


### ⚠ BREAKING CHANGES

* add job templates for pod sizing

### Features

* add job templates for pod sizing ([13e7a80](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/13e7a8092f91685882796c9606ed04b1336392ab))


### Bug Fixes

* update kaniko to 1.9.1 ([f235981](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/f235981a50a106061e1911948d5c5c8fc0ea71c0))

## [7.3.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v7.2.0...v7.3.0) (2022-10-24)


### Features

* add author email domain checking to commitlint job ([97bf755](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/97bf7557e277265bb606600b1cbeae6815705ce2))
* disable cache for templates that definitely won't be using it ([a62e3b8](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/a62e3b829e564ed2cb680d5a1956ff42e28145fb))
* specify default s tag for cobertura job ([094a3d7](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/094a3d789f5e54de1c9607c75b9b5d730fda2740))


### Refactoring

* add base job for notify template definitions ([624fec0](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/624fec03bc716a6420f1bf79ea0ee4e0967e3ac8))


### Documentation

* document default size configuration in template directory ([ca937e7](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/ca937e7603a2989ec4698a26c7adcef9e0aa95c8))

## [7.2.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v7.1.0...v7.2.0) (2022-10-17)


### Features

* add templates for slack build notifications ([9cab4e1](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/9cab4e1a1e12a4ed07673829271cd5df176deaa5))
* bump base images to 3.4.0 (updated standard-version) ([aa891dd](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/aa891ddfea0947b1b4890b6e02a8951b1263aee1))
* configure standard-version with all available commit types ([4dced03](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/4dced03b227322eb8c7106b01554fe1afd298b99))
* extract jsn version as a common variable ([c4a1e5e](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/c4a1e5e8b241e0607f992358e0904457455ba4c7))


### CI

* use slack notifications for releasing self ([060890a](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/060890a93d64722606d1d4841358851a3a12d3a9))


### Documentation

* fixup documentation about jacoco-to-cobertura ([ad010a2](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/ad010a21577ed6df1cb82041e52fc332400387a5))

## [7.1.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v7.0.0...v7.1.0) (2022-10-13)


### Features

* add ability to create GitLab releases for pre-releases ([e2005b2](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/e2005b2cebd289f3706257e84a735a611c48e87a))

## [7.0.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v6.9.0...v7.0.0) (2022-06-16)


### ⚠ BREAKING CHANGES

* parallel-on-demand triggering on project and common changes

### Bug Fixes

* parallel-on-demand triggering on project and common changes ([cfb5f97](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/cfb5f97ff6eaa05699bfbf4db27c8f6f96dda89e))

## [6.9.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v6.8.1...v6.9.0) (2022-06-09)


### Features

* add jacoco to cobertura coverage converter template ([8180b7e](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/8180b7ea232ff6117f2cb44aa8cd0ed60b6e2247))

### [6.8.1](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v6.8.0...v6.8.1) (2022-04-20)


### Bug Fixes

* add node cache error handling ([70ed96a](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/70ed96a2b786e54cf1c946cb1fd41aab8c65181e))

## [6.8.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v6.7.0...v6.8.0) (2022-04-11)


### Features

* add variables to determine release type in on-release jobs ([cbd24f1](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/cbd24f15f882757185eb8b9e1b5aee83ff8aa533))

## [6.7.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v6.6.0...v6.7.0) (2022-03-31)


### Features

* add docker build step using kaniko ([bb19e61](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/bb19e61c0e18beed5f1263437855f3a3d84e7af1))
* add template for parallel docker image builds ([54f1444](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/54f1444f413e3f968b2ea41a975127113c0b502c))
* externalize retry configuration to .jarvis-retry-default ([90e4169](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/90e4169c1a4cb965c8aade84016d32e68527dc9f))


### Bug Fixes

* docker image version calculation ([a2cf056](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/a2cf0566e3b13f2cd294b47aea5db94439b68fd0))
* use parallel base template for docker lint template ([5bd42ab](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/5bd42ab6ef6a1d240f1761d1b5b9210709cf9303))

## [6.6.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v6.5.0...v6.6.0) (2022-03-29)


### Features

* make PROJECT_DIR variable available for parallel jobs ([ed67bea](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/ed67beafe0b69e920e43796822a3d625b76d528d))
* update docker linting, add parallel docker linting ([f04667b](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/f04667b681fa0cd5556c56ee61aafb2f694a8031))

## [6.5.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v6.4.0...v6.5.0) (2022-03-25)


### Features

* introduce node caching configuration and preheat job template ([a20040b](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/a20040b907a4af25b320e8b2c76cc05af191a6ec))

## [6.4.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v6.3.0...v6.4.0) (2022-03-22)


### Features

* bump updated base image versions ([d041a12](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/d041a12b203e55d28485a4c95521f7412fe9a30e))

## [6.3.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v6.2.2...v6.3.0) (2022-03-18)


### Features

* add option to enforce manual versioning scheme ([bbeeb63](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/bbeeb635f50be56e499b5f162e22eddca0a48999))
* add parallel job templates ([7e899d8](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/7e899d88f066227a2d571530bbd7ce127868a947))
* introduce base MR template ([6d66965](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/6d669658e964817fa40a0fcf708b3fa6dee79380))

### [6.2.2](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v6.2.1...v6.2.2) (2022-01-13)


### Bug Fixes

* bump updated base image versions ([9e14495](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/9e14495066aa32773ad5c7dc2843171353add717))

### [6.2.1](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v6.2.0...v6.2.1) (2021-11-29)


### Bug Fixes

* suppress error when known_hosts doesn't exist ([3557225](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/355722506017b6c70fbb23168146032c4f57b34f))

## [6.2.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v6.1.0...v6.2.0) (2021-11-26)


### Features

* remove duplicated hosts ([14f8ae7](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/14f8ae7539092954d9ba3847f60d8a14b64e4e1a))
* set name and email in env vars ([d110151](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/d110151812ae4e79f68159d6b9aa3ca835a1f587))

## [6.1.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v6.0.0...v6.1.0) (2021-10-01)


### Features

* add new base image templates ([7419091](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/741909113cd739ad433170b62f4e3d92d081faef))


### Bug Fixes

* update base image versions for existing job templates ([a6c5f2f](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/a6c5f2fd2e4bf765769a6fac6b46ea8c3e4e87b2))

## [6.0.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v5.3.0...v6.0.0) (2021-09-14)


### ⚠ BREAKING CHANGES

* refactor Git key and identity handling
* migrate project to OSS version hosted on gitlab.com

### Features

* migrate project to OSS version hosted on gitlab.com ([ceee152](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/ceee1522581bf54726d4c082a71303a320c04cd7))
* move separate files to a single jarvis.yml ([e22f40d](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/e22f40db5f4a77cff0ab6169a11ae67c543c7d83))
* refactor Git key and identity handling ([ceba8e7](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/ceba8e7e80676bce6c6faf57a0ddce95dec24655))
* upgrade to latest builds of helper images ([8d06049](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/8d06049907c11579df08d3cfbf1f56682bcaf91d))

## [5.3.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v5.2.0...v5.3.0) (2021-05-14)


### Features

* add .jarvis-extract-release-changes-since-deployment template ([4794b9f](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/4794b9f7bd14b86dfd6f5e023f1381e644de63bc))


### Bug Fixes

* do not run release jobs on scheduled pipelines ([62935ce](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/62935cee42490fe7008f490873d5ada6069f40c7))

## [5.2.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v5.1.0...v5.2.0) (2021-04-12)


### Features

* use default branch instead of hardcoding 'master' ([367fdd9](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/367fdd9f7509c5fbbeb1f1e0470decd4ad400950))

## [5.1.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v5.0.0...v5.1.0) (2021-03-19)


### Features

* allow overriding release brnach rules for main release ([0811954](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/081195489d6483827f04cd2a1ae202cd34ecdfb6))

## [5.0.0](https://gitlab.com/team-supercharge/jarvis/jarvis/compare/v4.2.0...v5.0.0) (2021-03-17)


### ⚠ BREAKING CHANGES

* remove helper image sources from repo
* remove pre- prefix from pre-release tags

### Features

* add extract-metadata step to access current changes and version ([25285a5](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/25285a5a085ebfcbefa15a26a86dd5c0e7c1079d))
* add RELEASE_AS option to .jarvis-main-release to manually set version ([f3dacb4](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/f3dacb4fe7e3c4c2a8fe91e6cb01ed89a6b6cce8))
* create a GitLab release for main releases ([8d66ebd](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/8d66ebd1868b7bcb11b310aa46414359555ead88))
* refactor shallow unshallow command with !reference tag ([0f42e78](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/0f42e7806d7d7d587ba07c0f35d9ad6c849e32f7))
* remove helper image sources from repo ([f3ec146](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/f3ec146868c42b4ddef34ea06b9abf790f525938))
* remove pre- prefix from pre-release tags ([0eaec7e](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/0eaec7e221965dae34f2335fb61f6f650db8950c))
* separate extract metadata script for version/changes ([28679ae](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/28679ae357188721d330eb8513d314ee971d68c8))


### Bug Fixes

* clean up standard-version CLI params ([34cc2f3](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/34cc2f3175b31817b4c08269abc64d1a4328b78d))
* move unshallow command to base release job ([d75307d](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/d75307d9e32a04aeb897d5ce8e59c1e127f14f30))
* refactor jarvis-on-* templates with base template ([4dfa730](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/4dfa730f948dd2cac288139162d1d1f3bee37f44))
* simplify pre-release version generation ([e7efda0](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/e7efda0596c4631d7e438eceaefa1c28686fb2f8))

## [4.2.0](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v4.1.0...v4.2.0) (2021-03-01)


### Features

* **deploy,release:** add git-lfs support ([3b51b70](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/3b51b703548f1fc8f3ddad8d58235e6acac5b982))
* **release:** change image to one with lfs support ([d41f1d2](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/d41f1d2039aea4c4e60f827666b3dc60c5e3dbe0))

## [4.1.0](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v4.0.0...v4.1.0) (2021-02-03)


### Features

* allow issue prefix and url format parameters ([9e941e9](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/9e941e99c67584ad55fb17a2564aaabd7a925d0e))
* extend pre-release with issue prefix and url format parameters ([0317e56](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/0317e56e7b5cbd37f4a4385a8616c547cb5d301f))


### Bug Fixes

* changelog generation in shallow repositories ([2defad3](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/2defad3cf040dbf967c05a564c70fce03996408f))

## [4.0.0](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v3.0.1...v4.0.0) (2020-11-27)


### ⚠ BREAKING CHANGES

* enable commiting for pre-releases, making handle-pre-release unnecessary

### Features

* enable commiting for pre-releases, making handle-pre-release unnecessary ([70eb2e2](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/70eb2e2a4c4471be8db71cf970a32fd10e5184d1))
* filtering release pipeline if tip of master is already a release ([e157fff](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/e157fffa048d22d1505e5e5004014b9865707195))


### Bug Fixes

* run .jarvis-on-* for tags only ([9578fda](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/9578fda01fe6bda32632a6327d4242795790137a))

### [3.0.1](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v3.0.0...v3.0.1) (2020-11-18)


### Bug Fixes

* replace problematic characters in prerelease tagname ([bf07471](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/bf07471a2f9f21f879896335360d7c19409d39bc))

## [3.0.0](https://gitlab.com/team-supercharge/jarvis/jarvis/-/compare/v2.0.0...v3.0.0) (2020-11-18)


### ⚠ BREAKING CHANGES

* correctly identify commits between releases

### Bug Fixes

* correctly identify commits between releases ([a2fe849](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/a2fe8492bd1f3d712eb5fbffbdf7118bb9139e87))

## 2.0.0 (2020-11-05)


### ⚠ BREAKING CHANGES

* rename prerelease jobs to pre-release
* use semver-compatible pre-release versioning
* define hidden jobs only

### Features

* add dependency security scanning for java projects ([a4c4abb](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/a4c4abb02b32a593f17d174e4dd4ca89d697227d))
* adds job dependency to other deploy helper images ([a62644b](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/a62644b6a0868f38cf7ababed81bf852963d7c4c))
* adds terraform to deploy helper image ([798c5b3](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/798c5b3a61626998fbbe33e6457ead6bcb0d2b87))
* build custom commitlint image ([b917d8f](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/b917d8f016ec93c4f2c117be3d5845788693b69e))
* creaet api-generator base image with node, java, jq ([083160b](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/083160bba3607de15986be0d8f178d11f6e56f17))
* define hidden jobs only ([3158413](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/3158413de75d4543fda85f2b83d32378a9be66bb))
* dockerfile linting ([f2f393d](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/f2f393d4bb6f9c72ca012fe647ea0b7285b4c748))
* jarvis based versioning and releases ([04e4407](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/04e44076303d6ecec6348b4e4ac2f5161e55c8b7))
* rename prerelease jobs to pre-release ([1fc697f](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/1fc697f93f8b644fa9794a143718fd2bfaf0cdac))
* use custom commitlint image ([a6e0da8](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/a6e0da8cc0550c66e266cd4cbdfbe2296e0d0a17))
* use semver-compatible pre-release versioning ([6925c46](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/6925c46118bc0abda12aabc9ec2f62fe9db75375))


### Bug Fixes

* remove invalid rule ([9ebe291](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/9ebe2910f81e7f0dcbec2a971c780f69873f169e))
* update git-push.yml command to support macOS ([b8becaf](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/b8becaf87e4d90ae7adcd9e094ff04ae70c23d30))
* updates generated url format for gitlab ([36455ba](https://gitlab.com/team-supercharge/jarvis/jarvis/commit/36455ba62b6ba952042f2b05224441d52db4ce4e))
